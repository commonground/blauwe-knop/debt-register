// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package debtregister

import "time"

type Schuld struct {
	typeSchuld                            string
	omschrijving                          string
	openstaandBedrag                      int
	datumOpenstaandBedragLaatstBijgewerkt time.Time
	vorderingOvergedragen                 bool
}

func NewSchuld(typeSchuld string, omschrijving string, openstaandBedrag int, datumOpenstaandBedragLaatstBijgewerkt time.Time, vorderingOvergedragen bool) *Schuld {
	return &Schuld{
		typeSchuld:                            typeSchuld,
		omschrijving:                          omschrijving,
		openstaandBedrag:                      openstaandBedrag,
		datumOpenstaandBedragLaatstBijgewerkt: datumOpenstaandBedragLaatstBijgewerkt,
		vorderingOvergedragen:                 vorderingOvergedragen,
	}
}

func (d Schuld) TypeSchuld() string {
	return d.typeSchuld
}

func (d Schuld) Omschrijving() string {
	return d.omschrijving
}

func (d Schuld) OpenstaandBedrag() int {
	return d.openstaandBedrag
}

func (d Schuld) DatumOpenstaandBedragLaatstBijgewerkt() time.Time {
	return d.datumOpenstaandBedragLaatstBijgewerkt
}

func (d Schuld) VorderingOvergedragen() bool {
	return d.vorderingOvergedragen
}
