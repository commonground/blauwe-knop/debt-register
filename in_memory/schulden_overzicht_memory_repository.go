// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package in_memory

import (
	"math"
	"math/rand"
	"strings"
	"time"

	"debtregister"
)

type SchuldenOverzicht struct {
	organisatie string
	schulden    []debtregister.Schuld
}

func NewSchuldenOverzicht(organisatie string) *SchuldenOverzicht {
	d := &SchuldenOverzicht{
		organisatie: organisatie,
	}

	return d
}
func (d *SchuldenOverzicht) Organisatie() string {
	return d.organisatie
}
func (d *SchuldenOverzicht) Schulden() []debtregister.Schuld {
	return d.schulden
}

func (d *SchuldenOverzicht) AddSchuld(typeSchuld string, omschrijving string, openstaandBedrag int, datumOpenstaandBedragLaatstBijgewerkt time.Time, vorderingOvergedragen bool) {
	schuld := debtregister.NewSchuld(typeSchuld, omschrijving, openstaandBedrag, datumOpenstaandBedragLaatstBijgewerkt, vorderingOvergedragen)
	d.schulden = append(d.schulden, *schuld)
}

var testData []*SchuldenOverzicht

func init() {
	testDataCjib := NewSchuldenOverzicht("CJIB")

	testDataCjib.AddSchuld(
		"WAHV Beschikkingen",
		"",
		int(172800),
		time.Date(2019, 06, 05, 0, 0, 0, 0, time.UTC),
		rand.Intn(2) == 1,
	)

	testDataCjib.AddSchuld(
		"Schadevergoedingsmaatregel",
		"",
		int(69725),
		time.Date(2019, 06, 05, 0, 0, 0, 0, time.UTC),
		rand.Intn(2) == 1,
	)

	testDataUwv := NewSchuldenOverzicht("UWV")

	testDataUwv.AddSchuld(
		"Teveel ontvangen WW-uitkering",
		"",
		int(170165),
		time.Date(2019, 06, 05, 0, 0, 0, 0, time.UTC),
		rand.Intn(2) == 1,
	)

	testDataUwv.AddSchuld(
		"Teveel ontvangen WAO-uitkering",
		"",
		int(179666),
		time.Date(2019, 06, 05, 0, 0, 0, 0, time.UTC),
		rand.Intn(2) == 1,
	)

	testDataBelastingdienst := NewSchuldenOverzicht("Belastingdienst")

	testDataBelastingdienst.AddSchuld(
		"Inkomstenbelasting 2018",
		"",
		int(1258400),
		time.Date(2019, 06, 05, 0, 0, 0, 0, time.UTC),
		rand.Intn(2) == 1,
	)

	testDataBelastingdienst.AddSchuld(
		"Verzuimboete",
		"",
		int(38500),
		time.Date(2019, 06, 05, 0, 0, 0, 0, time.UTC),
		rand.Intn(2) == 1,
	)

	testDataSvb := NewSchuldenOverzicht("SVB")

	testDataSvb.AddSchuld(
		"Teveel ontvangen AOW",
		"",
		int(356922),
		time.Date(2019, 06, 05, 0, 0, 0, 0, time.UTC),
		rand.Intn(2) == 1,
	)

	testDataDuo := NewSchuldenOverzicht("DUO")

	testDataDuo.AddSchuld(
		"Achterstallige betaling lening",
		"",
		int(198832),
		time.Date(2019, 06, 05, 0, 0, 0, 0, time.UTC),
		rand.Intn(2) == 1,
	)

	testDataWaterschapRivierenland := NewSchuldenOverzicht("Waterschap Rivierenland")

	testDataWaterschapRivierenland.AddSchuld(
		"Achterstand betaling waterschapsbelasting",
		"",
		int(41207),
		time.Date(2019, 06, 05, 0, 0, 0, 0, time.UTC),
		rand.Intn(2) == 1,
	)

	TestDataGemeenteUtrecht := NewSchuldenOverzicht("Gemeente Utrecht")

	TestDataGemeenteUtrecht.AddSchuld(
		"Onroerendezaakbelasting",
		"",
		int(128000),
		time.Date(2019, 06, 05, 0, 0, 0, 0, time.UTC),
		rand.Intn(2) == 1,
	)

	TestDataGemeenteUtrecht.AddSchuld(
		"Afvalstoffenheffing",
		"",
		int(60800),
		time.Date(2019, 06, 05, 0, 0, 0, 0, time.UTC),
		rand.Intn(2) == 1,
	)

	TestDataGemeenteUtrecht.AddSchuld(
		"Leges bouwvergunning",
		"",
		int(9600),
		time.Date(2019, 06, 05, 0, 0, 0, 0, time.UTC),
		rand.Intn(2) == 1,
	)

	testDataGemeenteZeewolde := NewSchuldenOverzicht("Gemeente Zeewolde")

	TestDataGemeenteUtrecht.AddSchuld(
		"Achterstallige betaling parkeergelden",
		"",
		int(9750),
		time.Date(2019, 06, 05, 0, 0, 0, 0, time.UTC),
		rand.Intn(2) == 1,
	)

	testData = append(
		testData,
		testDataCjib,
		testDataUwv,
		testDataBelastingdienst,
		testDataSvb,
		testDataDuo,
		testDataWaterschapRivierenland,
		TestDataGemeenteUtrecht,
		testDataGemeenteZeewolde,
	)
}

type overzichtRepository struct {
	schulden map[string][]debtregister.Schuld
}

func NewOverzichtRepository(organization string) *overzichtRepository {
	return &overzichtRepository{
		schulden: map[string][]debtregister.Schuld{
			"814859094": createSchulden(organization, 1),
			"309777938": createSchulden(organization, 1),
			"950053545": createSchulden(organization, 1),
			"761582915": createSchulden(organization, 1),
			"570957588": createSchulden(organization, 1),
		},
	}
}

func (d *overzichtRepository) FindByBSN(bsn *debtregister.BSN) ([]debtregister.Schuld, error) {
	result, ok := d.schulden[bsn.ToString()]
	if !ok {
		result = []debtregister.Schuld{}
	}

	return result, nil
}

func createSchulden(organization string, schuldSaldoMultiplier float64) []debtregister.Schuld {
	var result *SchuldenOverzicht

	for _, x := range testData {
		if x.Organisatie() == organization {
			result = x
			break
		}
	}

	if result == nil {
		result = NewSchuldenOverzicht(organization)

		if strings.HasPrefix(organization, "demo-") {

			result.AddSchuld(
				"Onroerendezaakbelasting",
				"",
				int(math.Round(40000*schuldSaldoMultiplier)),
				time.Date(2019, 06, 05, 0, 0, 0, 0, time.UTC),
				rand.Intn(2) == 1,
			)

			result.AddSchuld(
				"Afvalstoffenheffing",
				"",
				int(math.Round(19000*schuldSaldoMultiplier)),
				time.Date(2019, 06, 05, 0, 0, 0, 0, time.UTC),
				rand.Intn(2) == 1,
			)

			result.AddSchuld(
				"Leges bouwvergunning",
				"",
				int(math.Round(3000*schuldSaldoMultiplier)),
				time.Date(2019, 06, 05, 0, 0, 0, 0, time.UTC),
				rand.Intn(2) == 1,
			)
		}
	}

	return result.Schulden()
}
