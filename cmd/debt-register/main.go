// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package main

import (
	"fmt"
	"log"
	"net/http"

	"go.uber.org/zap"

	"debtregister"
	http_infra "debtregister/http"
	"debtregister/in_memory"
)

type options struct {
	ListenAddress string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:8088" description:"Address for the debt register to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	Organization  string `long:"organization" env:"ORGANIZATION" description:"Organization name"`
	APIKey        string `long:"api-key" env:"API_KEY" default:"" description:"Key to protect the API endpoints."`

	LogOptions
}

func main() {

	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	if len(cliOptions.Organization) < 1 {
		log.Fatalf("please specify an organization")
	}

	logger, err := newZapLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	overzichtRepository := in_memory.NewOverzichtRepository(cliOptions.Organization)
	schuldenUseCase := debtregister.NewSchuldenUseCase(overzichtRepository)
	router := http_infra.NewRouter(schuldenUseCase, cliOptions.APIKey)

	logger.Info(fmt.Sprintf("start listening on %s", cliOptions.ListenAddress))
	err = http.ListenAndServe(cliOptions.ListenAddress, router)
	if err != nil {
		if err != http.ErrServerClosed {
			panic(err)
		}
	}
}

func newZapLogger(logOptions LogOptions) (*zap.Logger, error) {
	config := logOptions.ZapConfig()
	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return logger, nil
}
