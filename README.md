# Debt Register

Debt register written in golang.

## Installation

Prerequisites:

- [Git](https://git-scm.com/)
- [Golang](https://golang.org/doc/install)
- [Modd](https://github.com/cortesi/modd)

1. Download the required Go dependencies:

```sh
go mod download
```

1. Start the debt register:

```sh
go run cmd/debt-register/main.go --organization demo-org
```

Or run the debt register using modd, which wil restart the debt register on file changes.

```sh
modd
```

By default, the debt register will run on port `8080`.

## Adding mocks

We use [GoMock](https://github.com/golang/mock) to generate mocks.
When you make updates to code for which there are mocks, you should regenerate the mocks.

**Regenerating mocks**

```sh
sh regenerate-gomock-files.sh
```

## Deployment

Prerequisites:

- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [helm](https://helm.sh/docs/intro/)
- deployment access to the haven cluster `dv-prod-common`

You will need to have a [Redis](https://redis.io/) database running on the cluster.
If you do not have Redis running on the cluster you can use `helm` to install it.

First add the chart to `helm`

```
helm repo add bitnami https://charts.bitnami.com/bitnami
```

Now use `helm` to deploy the Redis chart on the cluster

```
helm install -n bk-test redis bitnami/redis
```

Once Redis is runnning you can use `helm` to deploy the debt register

```
helm upgrade --install debt-register ./charts/debt-register -n bk-test
```
