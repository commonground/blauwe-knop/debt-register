// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"context"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"

	"debtregister"
)

func NewRouter(schuldenUseCase *debtregister.SchuldenUseCase, apiKey string) *chi.Mux {
	r := chi.NewRouter()

	r.Use(middleware.Logger)

	if apiKey != "" {
		r.Use(authenticatedOnly(apiKey))
	}

	r.Route("/api/v1/", func(r chi.Router) {
		addOverzichtRoutes(r, schuldenUseCase)
	})

	healthCheckHandler := healthcheck.NewHandler("debt-register", []healthcheck.Checker{})
	r.Route("/health", func(r chi.Router) {
		r.Get("/", healthCheckHandler.HandleHealth)
		r.Get("/check", healthCheckHandler.HandlerHealthCheck)
	})

	return r
}
func addOverzichtRoutes(router chi.Router, schuldenUseCase *debtregister.SchuldenUseCase) {
	router.Post("/schulden",
		func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), schuldenUseCaseKey, schuldenUseCase)
			handlerOverzicht(w, r.WithContext(ctx))
		})
}

func authenticatedOnly(apiKey string) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			providedApiKey := r.Header.Get("Authentication")

			if providedApiKey != apiKey {
				log.Printf("Unauthorized: apikey unvalid")
				http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
				return
			}
			next.ServeHTTP(w, r)
		})
	}
}

type key int

const (
	schuldenUseCaseKey key = iota
)
