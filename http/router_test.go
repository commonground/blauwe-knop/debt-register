// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http_test

import (
	"bytes"
	"errors"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"debtregister"
	http_infra "debtregister/http"
	"debtregister/mock"
)

const MockBsn = "814859094"

func Test_CreateRouter_debtregisteroverzicht(t *testing.T) {
	type fields struct {
		overzichtRepository debtregister.OverzichtRepository
	}
	type args struct {
		BSN string
	}
	tests := []struct {
		name                   string
		fields                 fields
		args                   args
		expectedHTTPStatusCode int
		expectedBody           string
	}{
		{
			"without bsn",
			fields{
				func() debtregister.OverzichtRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockOverzichtRepository(ctrl)

					return repo
				}(),
			},
			args{
				"",
			},
			http.StatusBadRequest,
			"failed parse bsn\n",
		},
		{
			"find by BSN fails",
			fields{
				func() debtregister.OverzichtRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					bsnModel, _ := debtregister.NewBSN("814859094")

					schuld := debtregister.NewSchuld(
						"Onroerende zaakbelasting",
						"",
						100,
						time.Date(2019, 06, 05, 0, 0, 0, 0, time.UTC),
						false,
					)

					repo := mock.NewMockOverzichtRepository(ctrl)
					repo.EXPECT().FindByBSN(bsnModel).Return([]debtregister.Schuld{*schuld}, errors.New("error")).AnyTimes()

					return repo
				}(),
			},
			args{
				MockBsn,
			},
			http.StatusInternalServerError,
			"unable to get debtregisteroverzicht\n",
		},
		{
			"with valid bsn",
			fields{
				func() debtregister.OverzichtRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					bsnModel, _ := debtregister.NewBSN("814859094")

					schuld := debtregister.NewSchuld(
						"Onroerende zaakbelasting",
						"",
						100,
						time.Date(2019, 06, 05, 0, 0, 0, 0, time.UTC),
						false)

					repo := mock.NewMockOverzichtRepository(ctrl)
					repo.EXPECT().FindByBSN(bsnModel).Return([]debtregister.Schuld{*schuld}, nil).AnyTimes()

					return repo
				}(),
			},
			args{
				MockBsn,
			},
			http.StatusOK,
			"[{\"typeSchuld\":\"Onroerende zaakbelasting\",\"omschrijving\":\"\",\"openstaandBedrag\":100,\"datumOpenstaandBedragLaatstBijgewerkt\":\"2019-06-05T00:00:00Z\",\"vorderingOvergedragen\":false}]\n",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			debtregisterUseCase := debtregister.NewSchuldenUseCase(test.fields.overzichtRepository)
			router := http_infra.NewRouter(debtregisterUseCase, "")
			w := httptest.NewRecorder()

			requestBody := "{\"bsn\": \"" + test.args.BSN + "\"}\n"
			requestBodyBytes := []byte(requestBody)
			request := httptest.NewRequest("POST", "/api/v1/schulden", bytes.NewReader(requestBodyBytes))
			request.Header.Set("Authorization", "")

			router.ServeHTTP(w, request)

			resp := w.Result()
			body, _ := ioutil.ReadAll(resp.Body)

			assert.Equal(t, test.expectedHTTPStatusCode, resp.StatusCode)
			assert.Equal(t, test.expectedBody, string(body))
		})
	}
}
