// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"fmt"
	"log"
	"net/http"

	"github.com/go-chi/render"

	"debtregister"
)

type requestGetSchulden struct {
	BSN string `json:"bsn"`
}

func handlerOverzicht(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()
	schuldenUseCase, _ := ctx.Value(schuldenUseCaseKey).(*debtregister.SchuldenUseCase)

	requestPayload := &requestGetSchulden{}
	err := render.DecodeJSON(req.Body, requestPayload)
	if err != nil {
		log.Printf("failed to read request payload: %v", err)
		http.Error(w, fmt.Sprintf("failed to read request payload: %v", err), http.StatusBadRequest)
		return
	}

	bsn, err := debtregister.NewBSN(requestPayload.BSN)
	if err != nil {
		log.Printf("failed parse bsn: %v", err)
		http.Error(w, "failed parse bsn", http.StatusBadRequest)
		return
	}

	schulden, err := schuldenUseCase.OverzichtByBsn(*bsn)
	if err != nil {
		log.Printf("unable to get debtregister overzicht for bsn: %v", err)
		http.Error(w, "unable to get debtregisteroverzicht", http.StatusInternalServerError)
		return
	}

	viewModel := generateSchuldViewModel(schulden)
	render.JSON(w, req, &viewModel)
}
