// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"time"

	"debtregister"
)

type schuld struct {
	TypeSchuld                            string `json:"typeSchuld"`
	Omschrijving                          string `json:"omschrijving"`
	OpenstaandBedrag                      int    `json:"openstaandBedrag"`
	DatumOpenstaandBedragLaatstBijgewerkt string `json:"datumOpenstaandBedragLaatstBijgewerkt"`
	VorderingOvergedragen                 bool   `json:"vorderingOvergedragen"`
}

func generateSchuldViewModel(schulden []debtregister.Schuld) []schuld {
	var schuldenReponse []schuld
	for _, v := range schulden {
		schuldenReponse = append(schuldenReponse, schuld{
			TypeSchuld:                            v.TypeSchuld(),
			Omschrijving:                          v.Omschrijving(),
			OpenstaandBedrag:                      v.OpenstaandBedrag(),
			DatumOpenstaandBedragLaatstBijgewerkt: v.DatumOpenstaandBedragLaatstBijgewerkt().Format(time.RFC3339),
			VorderingOvergedragen:                 v.VorderingOvergedragen(),
		})
	}

	return schuldenReponse
}
