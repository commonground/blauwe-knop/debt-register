// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package debtregister

import (
	"errors"
)

type OverzichtRepository interface {
	FindByBSN(bsn *BSN) ([]Schuld, error)
}

var ErrUnknownOverzicht = errors.New("no debtregister overzicht available")
var ErrEmptyBsn = errors.New("invalid bsn")

type SchuldenUseCase struct {
	overzichtRepository OverzichtRepository
}

func NewSchuldenUseCase(overzichtRepository OverzichtRepository) *SchuldenUseCase {
	return &SchuldenUseCase{
		overzichtRepository,
	}
}

func (d *SchuldenUseCase) OverzichtByBsn(bsn BSN) ([]Schuld, error) {
	result, err := d.overzichtRepository.FindByBSN(&bsn)
	return result, err
}
