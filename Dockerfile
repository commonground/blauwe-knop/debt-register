# Use go 1.x based on alpine image.
FROM golang:1.17.3-alpine AS build

ADD . /go/src/debt-register/
ENV GO111MODULE on
WORKDIR /go/src/debt-register
RUN go mod download
RUN go build -o dist/bin/debt-register ./cmd/debt-register

# Release binary on latest alpine image.
FROM alpine:latest

COPY --from=build /go/src/debt-register/dist/bin/debt-register /usr/local/bin/debt-register

# Add non-priveleged user
RUN adduser -D -u 1001 appuser
USER appuser
CMD ["/usr/local/bin/debt-register"]
